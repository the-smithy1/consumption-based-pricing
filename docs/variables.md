#Variables

- Audience: <code>Potential engineers and collaboraters for an upcoming Hackathon</code>
- Motif: <code>Highlander</code>
- Tone:  <code>playful and inspiring</code>
- Key Insights: <code>Importance of collaboration, innovation, and creativity to InnerSource</code>
- Goal of Warmer:  <code>There is a technical barrier to entry for most hackathons</code>
- Current Belief:  <code>InnerSource collaboration requires development skills and engineering background</code>
- New Perspective/Idea:  <code>InnerSource unlocks business advantage through technology, instead of investing in tools alone.</code>
- Objective of Reframe:  <code>Challenge the prevailing understanding of InnerSource as overly technical</code>
- Topic or Issue:
- Key Data Points & Insights:
- Desired Realization:
- Desired Emotional Response:
- Real-life Consequences:
- Background information:
- Current Belief or Practice:  *Community, collaboration, and technology*
- Desired Outcome:  *Transformative potential in reshaping business processes through InnerSource*
- Unique Value Proposition:  *If we build it, they won't come.  If they build it, they will love it!*
